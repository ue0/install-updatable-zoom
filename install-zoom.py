import os
import shutil
import tempfile
import subprocess
import urllib.request
from apt import auth
from apt import debfile

def apt_key_add(url_key):
    try:
        with urllib.request.urlopen(url_key) as k:
            auth.add_key(k.read().decode('utf-8'))
    except:
        print('broken url (key) ', url_key)

def dpkg_deb_url_install(url_deb):
    try:
        with urllib.request.urlopen(url_deb) as deb_obj:
            with tempfile.NamedTemporaryFile(suffix='.deb', delete=False) as deb_tmp:
                shutil.copyfileobj(deb_obj, deb_tmp)
            deb = debfile.DebPackage(deb_tmp.name)
            deb.install()
            subprocess.run(['apt-get', 'install', '-f', '-y'], check=False)
    except:
        print('Broken url ', url_deb)

apt_key_add('https://zoom.us/linux/download/pubkey')
dpkg_deb_url_install('https://zoom.us/linux/latest/zoom_amd64.deb')

#creating task in anacron

shutil.copyfile('update-zoom.py', '/etc/cron.weekly/update-zoom.py')
with open('/etc/cron.weekly/update-zoom', 'w', encoding='utf-8') as f:
    f.write('#!/bin/sh\npython3 /etc/cron.weekly/update-zoom.py\n')
os.chmod('/etc/cron.weekly/update-zoom', 0O755)