#!/usr/bin/env python3
from apt import cache, debfile
import tempfile
import urllib.request
import shutil
import subprocess

try:
    with urllib.request.urlopen('https://zoom.us/linux/latest/zoom_amd64.deb') as deb_obj:
        with tempfile.NamedTemporaryFile(suffix='.deb', delete=False) as deb_tmp:
            shutil.copyfileobj(deb_obj, deb_tmp)
        deb = debfile.DebPackage(deb_tmp.name)
        if deb.compare_to_version_in_cache() in (0, 3):
            deb.install()
            subprocess.run(['apt-get', 'install', '-f', '-y'], check=False)
            print('zoom is updated')
        else:
            print('you have up to date version of zoom')
except:
    print('Broken url ', url_deb)
