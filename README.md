# install-updatable-zoom

zoom on ubuntu cannot update itself and has no official package repository, so I wrote a script that can install zoom and update it once a week with anacron. works on ubuntu (16.04+), but can also work on debian (need only anacron and [python-apt](https://packages.debian.org/search?keywords=python-apt) packages).
